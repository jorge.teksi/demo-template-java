/**
 * View Models used by Spring MVC REST controllers.
 */
package mx.teksi.web.rest.vm;
