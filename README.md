# Plantilla de aplicacion Java

Plantilla de aplicacion Java con Spring Boot

## Desarrollo

Para ejecutar la aplicación:

```
./mvnw
```

## Preparar la aplicacion para producción

### Empaquetando jar

Para construir el paquete jar optimizado para su despliegue en producción ejecutar:

```

./mvnw -Pprod clean verify


```

Para ejecutar y probar el jar:

```

java -jar target/*.jar


```

### Empaquetando war

Para construir el paquete jar optimizado para su despliegue en producción en servidor de aplicaciones ejecutar:


```

./mvnw -Pprod,war clean verify


```

## Testing

Para ejecutar las pruebas de la aplicación:

```
./mvnw verify
```


### Calidad de codigo

Sonar es usado para las pruebas estaticas de codigo. Ejecutar el servidor de sonar (accesible en la direccion http://localhost:9001):

```
docker-compose -f src/main/docker/sonar.yml up -d
```

Ejecutar el analisis con Sonar:

```
./mvnw -Pprod clean verify sonar:sonar
```


## Usando docker en desarrollo (opcional)

Hay algunas configuraciones de docker-compose en el directorio [src/main/docker](src/main/docker) para ejecutar servicios

Por ejemplo para ejecutar mysql:

```
docker-compose -f src/main/docker/mysql.yml up -d
```

 Para detener el servicio y quitar el contenedor:

```
docker-compose -f src/main/docker/mysql.yml down
```

Para ejecutar la aplicacion en contenedor y todos los servicios que dependen de el hay que crear la image ejecutando:

```
./mvnw -Pprod verify jib:dockerBuild
```

Despues ejecutar:

```
docker-compose -f src/main/docker/app.yml up -d
```


## Integracion Continua (optional)

Para configurar CI en la aplicacion usar la plantilla [.gitlab-ci.yml][]
